'use strict';
const
    baseV = 2,
    colors = ['red', 'blue', 'yellow', 'magenta'],
    fps = 60;
let
    data = {
        snake: {
            pos: [0, 0],
            v: baseV,
            score: 0,
            trail: [],
            time: new Date().getTime(),
            direction: null,
            snakeV: [0, 0],
            id: null,
            fed: false,
            color: 'red'
        },
        foodPos: [-10, -10],
        prefs: {
            tickTime: 1000/24,
            gridSize: 1000,
            downed: false
        },
        snakes: []
    },
    prevFoodPos = [],
    online = true,
    servComm,
    servTick = null,
    tickId = null;

/* eslint-disable indent */

onmessage = function (input) {
    const inData = input.data;
    switch (typeof inData){
        case 'number':
            switch (inData){
                case 37:
                    left();
                    break;
                case 38:
                    up();
                    break;
                case 39:
                    right();
                    break;
                case 40:
                    down();
                    break;
                /*default:
                    console.log(evt.keyCode);
                    break;*/
            }
            break;
        case 'object':
            if (tickId !== null){
                clearInterval(tickId);
            }
            if (servTick !== null){
                clearInterval(servTick);
            }
            data = inData;
            if (input.ports[0]) servComm = input.ports[0];
            //gameTick();
            //self.postMessage(data);
            tickId = setInterval(gameTick, 1000/fps);
            servTick = setInterval(servSend, data.prefs.tickTime);
            if (online && !data.prefs.downed) {
                serverComm();
            }
            break;
    }
};

/* eslint-enable indent */

function main(){
    if (tickId !== null){
        clearInterval(tickId);
    }
    if (servTick !== null){
        clearInterval(servTick);
    }
    tickId = setInterval(gameTick, 1000/fps);
    servTick = setInterval(servSend, data.prefs.tickTime);
    console.log('background thread init');
}

function gameTick(){
    foodTick();
    snakesTick();
    self.postMessage(data);
}

function foodTick() {
    if (!data.snake.fed && (Math.abs(data.snake.pos[0]-data.foodPos[0]) < 10) && (Math.abs(data.snake.pos[1]-data.foodPos[1]) < 10)) {
        data.snake.score++;
        data.snake.v = baseV; //+ snake.score/10;
        foodPosChange();
    }
}

function snakesTick(){
    data.snake = mngSnake(data.snake);
    data.snake.time = new Date().getTime();
    trailCheck(data.snake);

    data.snakes.forEach(function (snek, i, a) {
        a[i] = mngSnake(snek);
        trailCheck(snek);
    });

}

function foodPosChange(){
    prevFoodPos = data.foodPos;
    data.foodPos = [-10, -10];
    if (online){
        data.snake.fed = true;
    }
    else {
        data.foodPos = randomPos();
    }
}

function randomPos(){
    return [Math.floor((Math.random() * (data.prefs.gridSize-10)) + 10), Math.floor((Math.random() * (data.prefs.gridSize-10)) + 10)];
}

function trailCheck(snek){
    snek.trail.forEach(function (p) {
        if(Math.abs(data.snake.pos[0] - p[0]) < 2 && Math.abs(data.snake.pos[1] - p[1])<2){
            data.snake.score = 0;
            data.snake.v = baseV;
            data.snake.pos = randomPos();
        }
    });
}

function mngSnake(snek){
    snek.pos[0]+=snek.snakeV[0];
    snek.pos[1]+=snek.snakeV[1];
    if (snek.pos[0] < 0){
        snek.pos[0] = data.prefs.gridSize;
    }
    if (snek.pos[1] < 0){
        snek.pos[1] = data.prefs.gridSize;
    }
    if (snek.pos[0] > data.prefs.gridSize){
        snek.pos[0] = 0;
    }
    if (snek.pos[1] > data.prefs.gridSize){
        snek.pos[1] = 0;
    }
    snek.trail.push([snek.pos[0]-snek.snakeV[0], snek.pos[1]-snek.snakeV[1]]);
    while(snek.trail.length > snek.score*10) {
        snek.trail.shift();
    }
    return snek;
}

function left(){
    if (data.snake.direction !== 3) {
        data.snake.snakeV = [-data.snake.v, 0];
        data.snake.direction = 1;
    }
}
function right(){
    if (data.snake.direction !== 1){
        data.snake.snakeV = [data.snake.v, 0];
        data.snake.direction = 3;
    }
}
function up(){
    if (data.snake.direction !== 4){
        data.snake.snakeV = [0, -data.snake.v];
        data.snake.direction = 2;
    }
}
function down(){
    if (data.snake.direction !== 2){
        data.snake.snakeV = [0, data.snake.v];
        data.snake.direction = 4;
    }
}

function serverComm(){
    servComm.onmessage = function (e) {
        const tdata = e.data;
        data.snakes = tdata['snakes'];
        if (!data.prefs.downed) {
            data.prefs = tdata['prefs'];
        }
        if (data.snake.id === null){
            data.snake.id = tdata['snakeId'];
            //colorId = snake.id - Math.floor(snake.id / colors.length) * colors.length;
            data.snake.color = colors[data.snake.id - Math.floor(data.snake.id / colors.length) * colors.length];
        }
        const fp = tdata['foodPos'];
        if (!(fp[0] === prevFoodPos[0] && fp[1] === prevFoodPos[1])) {
            data.foodPos = fp;
            data.snake.fed = false;
        }
        //bgThread.postMessage(data);
        //servComm.postMessage(snake);
    };
    main();
}

async function servSend() {
    servComm.postMessage(data.snake);
}