'use strict';
let
    webSock = null,
    snakeId = null,
    bgThread
;

onmessage = function (e) {
    let data = e.data;
    if (typeof data === 'string'){
        if (e.ports[0]) bgThread = e.ports[0];
        webSock = new WebSocket(data);
        console.log(`WebSocket connected to ${data}`);
        webSock.onmessage = function (msg) {
            let data = JSON.parse(msg.data);
            if (snakeId === null){
                for (let i = 0; snakeId === null; i++){
                    if (!(i.toString() in data['snakes'])){
                        snakeId = i.toString();
                    }
                }
            }
            delete data['snakes'][snakeId];
            data['snakeId'] = snakeId;
            let snakes = [];
            let recv = data['snakes'];
            Object.keys(recv).forEach(function (snekId){
                let cSnake = recv[snekId];
                cSnake['id'] = snekId;
                snakes.push(cSnake);
            });
            data['snakes'] = snakes;
            //self.postMessage(data);
            bgThread.postMessage(data);
        };
        bgThread.onmessage = function (e) {
            if (webSock.OPEN){
                webSock.send(JSON.stringify(e.data));
            }
        };
    }
    else if (webSock.OPEN) {
        webSock.send(JSON.stringify(data));
    }
};