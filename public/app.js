'use strict';
const
    servComm = new Worker('servComm.js'),
    bgThread = new Worker('bgThread.js'),
    channel = new MessageChannel(),
    baseV = 2;
let
    touch = true,
    canv = null,
    c = null,
    ctrl = null,
    ctrlCanv = null,
    data = {
        snake: {
            pos: [0, 0],
            v: baseV,
            score: 0,
            trail: [],
            time: new Date().getTime(),
            direction: null,
            snakeV: [0, 0],
            id: null,
            fed: false,
            color: 'red'
        },
        foodPos: [-10, -10],
        prefs: {
            tickTime: 1000/24,
            gridSize: 1000,
            downed: false
        },
        snakes: []
    },
    ctrlUnit = 0,
    ctrlPadX = 0,
    ctrlPadY = 0,
    procIds = [],
    online = true,
    go = true;

//VC = Virtual Canvas
let vc = {
    padX: 0,
    padY: 0,
    scale: 1,
    getSize: function (canvas) {
        if (canvas.width > canvas.height){
            vc.padY = 0;
            vc.padX = (canvas.width-canvas.height)/2;
            vc.scale = canvas.height / data.prefs.gridSize;
        }
        else {
            vc.padX = 0;
            vc.padY = (canvas.height-canvas.width)/2;
            vc.scale = canvas.width / data.prefs.gridSize;
        }
    }
};

function start(){
    bgThread.onmessage = function(input){
        data = input.data;
    };
    bgThread.postMessage(data, [channel.port1]);

    go = true;
    online = checkConn();
    canv = document.getElementById('snek');
    if (!online){
        data.foodPos = randomPos();
    }
    data.snake.pos = [data.prefs.gridSize/2, data.prefs.gridSize/2];
    data.snake.snakeV = [0, 0];
    data.snake.v = baseV;

    ctrlCanv = document.getElementById('ctrl');
    ctrl = ctrlCanv.getContext('2d');
    c = canv.getContext('2d');
    document.addEventListener('keydown', keyPush);
    resizeCanvas();
    window.addEventListener('resize', resizeCanvas, false);
    ctrlCanv.addEventListener('touchstart', touchHandler, false);
    canv.addEventListener('touchstart',async function(){
        if (!touch){
            touch = true;
            resizeCanvas();
        }
    },false);
    drawCtrl();
    draw();
    if (online && !data.prefs.downed) {
        serverComm();
    }
}

window.onload = start;

function draw() {
    if (go){
        requestAnimationFrame(draw);
    }

    c.clearRect(0, 0, canv.width, canv.height);
    c.beginPath();

    c.strokeStyle = 'white';
    c.rect(vc.padX, vc.padY, data.prefs.gridSize*vc.scale, data.prefs.gridSize*vc.scale);
    c.stroke();

    //Snakes
    data.snakes.forEach(function (snek, i, a){
        drawASnake(a[i]);
    });
    drawASnake(data.snake);
    //Food
    drawSquare('green', data.foodPos[0], data.foodPos[1]);
    //Score
    c.fillStyle = 'blue';
    c.font = '30px sans-serif';
    c.fillText(data.snake.score, 15, 40);
}

function drawCtrl(){
    const cW = ctrlCanv.width;
    const cH = ctrlCanv.height;
    ctrl.clearRect(0, 0, cW, cH);
    const t = cH / 3;
    ctrlUnit = cW / 4;
    if(ctrlUnit>t) {
        ctrlUnit = t;
    }
    ctrlPadX = (cW - 3*ctrlUnit)/2;
    ctrlPadY = (cH - 2*ctrlUnit)/2;
    ctrl.fillStyle = 'white';
    ctrl.fillRect(ctrlPadX+ctrlUnit,ctrlPadY,ctrlUnit,ctrlUnit);
    ctrl.fillRect(ctrlPadX,ctrlPadY+ctrlUnit,ctrlUnit,ctrlUnit);
    ctrl.fillRect(ctrlPadX+ctrlUnit,ctrlPadY+ctrlUnit,ctrlUnit,ctrlUnit);
    ctrl.fillRect(ctrlPadX+2*ctrlUnit,ctrlPadY+ctrlUnit,ctrlUnit,ctrlUnit);
}

function resizeCanvas() {
    if (touch){
        canv.width = window.innerWidth;
        canv.height = window.innerHeight/3*2;
        ctrlCanv.width = window.innerWidth;
        ctrlCanv.height = window.innerHeight/3;
        drawCtrl();
    }
    else {
        canv.width = window.innerWidth;
        canv.height = window.innerHeight;
        ctrlCanv.width = 0;
        ctrlCanv.height = 0;
    }
    vc.getSize(canv);
}

function drawSquare(color, posX, posY){
    c.fillStyle=color;
    c.fillRect(
        vc.padX + posX * vc.scale,
        vc.padY + posY * vc.scale,
        10*vc.scale, 10*vc.scale
    );
}

function randomPos(){
    return [Math.floor((Math.random() * (data.prefs.gridSize-10)) + 10), Math.floor((Math.random() * (data.prefs.gridSize-10)) + 10)];
}

function keyPush(evt) {
    if (touch){
        touch = false;
        resizeCanvas();
    }
    bgThread.postMessage(evt.keyCode);
}

async function touchHandler(evt) {
    const x = evt.touches[0].pageX;
    const y = evt.touches[0].pageY - canv.height;

    if (x<ctrlPadX+ctrlUnit){
        bgThread.postMessage(37);
    }
    else if(x>ctrlPadX+2*ctrlUnit){
        bgThread.postMessage(39);
    }
    else {
        if (y < ctrlPadY+ctrlUnit){
            bgThread.postMessage(38);
        }
        else {
            bgThread.postMessage(40);
        }
    }
}

async function drawASnake(s){
    s.trail.forEach(function (p) {
        drawSquare('white', p[0], p[1]);
    });
    drawSquare(s.color, s.pos[0], s.pos[1]);
}

function checkConn(){
    return ['http:', 'https:'].indexOf(window.location.protocol) > -1;
}

function serverComm(){
    if (window.location.protocol === 'https:'){
        servComm.postMessage(`wss://${window.location.host}`, [channel.port2]);
    }
    else{
        servComm.postMessage(`ws://${window.location.host}`, [channel.port2]);
    }
}

/* eslint-disable no-unused-vars */

function stop(){
    go = false;
    procIds.forEach(function (id){
        clearInterval(id);
    });
}