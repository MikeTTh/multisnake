module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "node": true
    },
    "parserOptions": {
        "ecmaVersion": 2017
      },
    "extends": "eslint:recommended",
    "rules": {
        'no-console': 'off',
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};