'use strict';
const
    express = require('express'),
    compression = require('compression'),
    path = require('path'),
    app = express(),
    WSServer = require('ws').Server,
    server = require('http').createServer();

let data = {
    snakes: {},
    foodPos: [-10, -10],
    prefs: {
        tickTime: 1000/20,  //the tick rate: edit the number after "1000/" the following way: 24 -> balanced, 24> -> for slow connetions, 24< -> for fast connections
        gridSize: 1000,     //the grid size: the minimum grid size is 20, because the game works with 10*10 blocks
        port: process.env.PORT || 5000,         //the server port: the default (5000) should be good, unless you are running other services on that port (http servers)

        downed: true        //downed must be true for snek to run.
    },
};

let fedRecv = false;

console.log("Starting server");

data.foodPos = randomPos();

let wss = new WSServer({
    server: server
});

server.on('request', app);

app.use([compression(), express.json(), express.static('public')]);

wss.on('connection', function (ws) {
    ws.send(JSON.stringify(data));
    ws.on('message', function (msg) {
        const recv = JSON.parse(msg);
        if (recv.id !== null){
            if (recv["fed"] && !fedRecv){
                fedRecv = true;
                data.foodPos = randomPos();
            }
            else if (recv["fed"] === false){
                fedRecv = false;
            }
            data.snakes[recv["id"]] = recv;
        }
        ws.send(JSON.stringify(data));
    });
});

app.post('/sendFood', async function(request, response){
    data.foodPos = randomPos();
    response.send(data.foodPos);
});

app.post('/testConn', async function(request, response) {
    response.send("OK");
});

app.get('/data.json', async function(req, res) {
    res.send(data);
});

setInterval(deleteOldSnakes, 1000);
setInterval(tick, 1000/data.prefs.tickTime);

async function tick() {
    checkFood();
}

async function deleteOldSnakes(){
    const time = (new Date).getTime();
    Object.keys(data.snakes).forEach(function (i) {
        if (data.snakes[i].time < (time-10000)){
            delete data.snakes[i];
        }
    });
}

async function checkFood(){
    //
}

function randomPos(){
    return [Math.floor((Math.random() * (data.prefs.gridSize - 10)) + 10), Math.floor((Math.random() * (data.prefs.gridSize - 10)) + 10)];
}

server.listen(data.prefs.port, function() {
    console.log(`http/ws server listening on ${data.prefs.port}`);
    console.log(`http://localhost:${data.prefs.port}`);
});
