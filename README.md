# MultiSnake  
The multiplayer snake game.

## Initializing  
To set up the repo after cloning, just run this in the repo folder:  
`npm i`  
## Starting  
To start the server run the following command in the repo folder:  
`npm start`  
And open the following in the browsers of players (substituting \<ip\> with the server's ip):  
`<ip>:8080`  
## Customization  
To customize settings, open `index.js` and edit the defaults in `data.prefs={}`


**_Have fun!_**